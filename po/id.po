# Indonesian translation for obfuscate.
# Copyright (C) 2020 obfuscate's COPYRIGHT HOLDER
# This file is distributed under the same license as the obfuscate package.
# Kukuh Syafaat <kukuhsyafaat@gnome.org>, 2020-2022.
#
msgid ""
msgstr ""
"Project-Id-Version: obfuscate master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/obfuscate/issues\n"
"POT-Creation-Date: 2021-11-28 13:51+0000\n"
"PO-Revision-Date: 2022-02-11 14:24+0700\n"
"Last-Translator: Kukuh Syafaat <kukuhsyafaat@gnome.org>\n"
"Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"

#: data/com.belmoussaoui.Obfuscate.desktop.in.in:3
#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:7
#: data/resources/ui/window.ui:151 src/application.rs:98
msgid "Obfuscate"
msgstr "Obfuscate"

#: data/com.belmoussaoui.Obfuscate.desktop.in.in:4
#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:8 src/application.rs:99
msgid "Censor private information"
msgstr "Sensor informasi pribadi"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/com.belmoussaoui.Obfuscate.desktop.in.in:10
msgid "Censor;Private;Image;Obfuscate;"
msgstr "Sensor;Pribadi;Gambar;Obfuscate;"

#: data/com.belmoussaoui.Obfuscate.gschema.xml.in:6
msgid "Default window maximized behaviour"
msgstr "Perilaku bawaan ketika jendela dimaksimalkan"

#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:10
msgid "Obfuscate lets you redact your private information from any image."
msgstr ""
"Obfuscate memungkinkan Anda menyusun informasi pribadi Anda dari gambar "
"apapun."

#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Jendela Utama"

#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:53
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Umum"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Open a New Window"
msgstr "Buka Jendela Baru"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Open a File"
msgstr "Buka Berkas"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Load Image from Clipboard"
msgstr "Muat Gambar dari Papan Klip"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Copy Image to Clipboard"
msgstr "Salin Gambar ke Papan Klip"

#: data/resources/ui/shortcuts.ui:38
msgctxt "shortcut window"
msgid "Save As"
msgstr "Simpan Sebagai"

#: data/resources/ui/shortcuts.ui:44
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Tampilkan Pintasan"

#: data/resources/ui/shortcuts.ui:50
msgctxt "shortcut window"
msgid "Close the Active Window"
msgstr "Tutup Jendela Aktif"

#: data/resources/ui/shortcuts.ui:58
msgctxt "shortcut window"
msgid "Editor"
msgstr "Penyunting"

#: data/resources/ui/shortcuts.ui:61
msgctxt "shortcut window"
msgid "Undo"
msgstr "Tidak Jadi"

#: data/resources/ui/shortcuts.ui:67
msgctxt "shortcut window"
msgid "Redo"
msgstr "Jadi lagi"

#: data/resources/ui/shortcuts.ui:73
msgctxt "shortcut window"
msgid "Zoom In"
msgstr "Zum Masuk"

#: data/resources/ui/shortcuts.ui:79
msgctxt "shortcut window"
msgid "Zoom Out"
msgstr "Zum Keluar"

#: data/resources/ui/window.ui:9
msgid "_Copy"
msgstr "Sa_lin"

#: data/resources/ui/window.ui:13
msgid "_Save"
msgstr "_Simpan"

#: data/resources/ui/window.ui:19 data/resources/ui/window.ui:41
msgid "_Open File"
msgstr "_Buka Berkas"

#: data/resources/ui/window.ui:23 data/resources/ui/window.ui:45
msgid "_New Window"
msgstr "Je_ndela Baru"

#: data/resources/ui/window.ui:29 data/resources/ui/window.ui:51
msgid "_Keyboard Shortcuts"
msgstr "Pintasan Papan Ti_k"

#: data/resources/ui/window.ui:33 data/resources/ui/window.ui:55
msgid "_About Obfuscate"
msgstr "Tent_ang Obfuscate"

#: data/resources/ui/window.ui:77
msgid "100%"
msgstr "100%"

#: data/resources/ui/window.ui:115 src/application.rs:165
msgid "Open"
msgstr "Buka"

#: data/resources/ui/window.ui:131
msgid "Drop an Image Here"
msgstr "Seret Gambar Di Sini"

#: src/application.rs:106
msgid "translator-credits"
msgstr "Kukuh Syafaat <kukuhsyafaat@gnome.org>, 2020-2022."

#: src/application.rs:162
msgid "Open File"
msgstr "Buka Berkas"

#: src/application.rs:166
msgid "Cancel"
msgstr "Batal"

#: src/application.rs:171
msgid "All Images"
msgstr "Semua Gambar"

#~ msgid "Default window x position"
#~ msgstr "Posisi jendela x bawaan"

#~ msgid "Default window y position"
#~ msgstr "Posisi jendela y bawaan"

#~ msgid "Censor private information."
#~ msgstr "Sensor informasi pribadi."

#~ msgid "@name-prefix@Obfuscate"
#~ msgstr "@name-prefix@Obfuscate"
