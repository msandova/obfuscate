use crate::config;
use crate::widgets::Window;
use adw::{prelude::*, subclass::prelude::*};
use gettextrs::gettext;
use gtk::{
    gio,
    glib::{self, clone},
    subclass::prelude::*,
};
use gtk_macros::action;
use log::info;

mod imp {
    use super::*;

    #[derive(Default, Debug)]
    pub struct Application {
        pub windows: gtk::WindowGroup,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "Application";
        type ParentType = adw::Application;
        type Type = super::Application;
    }

    impl ObjectImpl for Application {}
    impl ApplicationImpl for Application {
        fn startup(&self, app: &Self::Type) {
            self.parent_startup(app);

            app.style_manager().set_color_scheme(adw::ColorScheme::PreferDark);

            gtk::Window::set_default_icon_name(config::APP_ID);

            app.setup_actions();
        }

        fn activate(&self, app: &Self::Type) {
            self.parent_activate(app);
            let window = app.create_window();
            window.present();
            info!("Created application window.");
        }

        fn open(&self, application: &Self::Type, files: &[gio::File], _hint: &str) {
            for file in files.iter() {
                let window = application.create_window();
                window.open_file(file);
                window.present();
            }
        }
    }

    impl GtkApplicationImpl for Application {
        fn window_removed(&self, application: &Self::Type, window: &gtk::Window) {
            self.windows.remove_window(window);
            self.parent_window_removed(application, window);
        }
    }

    impl AdwApplicationImpl for Application {}
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)  @extends gio::Application, gtk::Application, adw::Application, gio::ActionMap;
}

impl Application {
    pub fn run() {
        info!("Obfuscate({})", config::APP_ID);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        // Create new GObject and downcast it into Application
        let app = glib::Object::new::<Application>(&[
            ("application-id", &Some(config::APP_ID)),
            ("flags", &gio::ApplicationFlags::HANDLES_OPEN),
            ("resource-base-path", &Some("/com/belmoussaoui/Obfuscate")),
        ])
        .unwrap();

        // Start running gtk::Application
        ApplicationExtManual::run(&app);
    }

    fn create_window(&self) -> Window {
        let window = Window::new(self);
        self.imp().windows.add_window(&window);
        window
    }

    fn show_about_dialog(&self) {
        gtk::AboutDialog::builder()
            .program_name(&gettext("Obfuscate"))
            .comments(&gettext("Censor private information"))
            .logo_icon_name(config::APP_ID)
            .license_type(gtk::License::Gpl30)
            .website("https://gitlab.gnome.org/World/obfuscate/")
            .version(config::VERSION)
            .transient_for(&self.window())
            .modal(true)
            .translator_credits(&gettext("translator-credits"))
            .authors(vec!["Bilal Elmoussaoui".into(), "Alexander Mikhaylenko".into()])
            .artists(vec!["Jakub Steiner".into(), "Tobias Bernard".into()])
            .build()
            .show();
    }

    fn window(&self) -> Window {
        self.active_window().expect("Failed to get a GtkWindow").downcast().unwrap()
    }

    fn setup_actions(&self) {
        // Quit
        action!(
            self,
            "quit",
            None,
            clone!(@weak self as app => move |_ , _| {
                app.window().close();
            })
        );
        // About
        action!(
            self,
            "about",
            None,
            clone!(@weak self as app => move |_, _| {
                app.show_about_dialog();
            })
        );
        // New Window
        action!(
            self,
            "new-window",
            None,
            clone!(@weak self as app => move |_, _| {
                let window = app.create_window();
                window.present();
            })
        );

        self.set_accels_for_action("app.new-window", &["<primary>n"]);
        self.set_accels_for_action("app.quit", &["<primary>q"]);
        self.set_accels_for_action("win.open", &["<primary>o"]);
        self.set_accels_for_action("win.save", &["<primary>s"]);
        self.set_accels_for_action("win.undo", &["<primary>z"]);
        self.set_accels_for_action("win.redo", &["<primary><shift>z"]);
        self.set_accels_for_action("win.paste", &["<primary>v"]);
        self.set_accels_for_action("win.copy", &["<primary>c"]);
        self.set_accels_for_action("win.zoom_in", &["<primary>plus"]);
        self.set_accels_for_action("win.zoom_out", &["<primary>minus"]);
        self.set_accels_for_action("win.show-help-overlay", &["<primary>question"]);
    }

    pub fn open_file(&self) {
        let open_dialog = gtk::FileChooserNative::new(
            Some(&gettext("Open File")),
            Some(&self.window()),
            gtk::FileChooserAction::Open,
            Some(&gettext("Open")),
            Some(&gettext("Cancel")),
        );
        open_dialog.set_modal(true);

        let all_filters = gtk::FileFilter::new();
        all_filters.set_name(Some(&gettext("All Images")));
        all_filters.add_mime_type("image/png");
        all_filters.add_mime_type("image/jpeg");
        all_filters.add_mime_type("image/bmp");

        let png_filter = gtk::FileFilter::new();
        png_filter.set_name(Some("image/png"));
        png_filter.add_mime_type("image/png");
        let jpg_filter = gtk::FileFilter::new();
        jpg_filter.set_name(Some("image/jpeg"));
        jpg_filter.add_mime_type("image/jpeg");
        let bpm_filter = gtk::FileFilter::new();
        bpm_filter.set_name(Some("image/bmp"));
        bpm_filter.add_mime_type("image/bmp");

        open_dialog.add_filter(&all_filters);
        open_dialog.add_filter(&png_filter);
        open_dialog.add_filter(&jpg_filter);
        open_dialog.add_filter(&bpm_filter);

        open_dialog.connect_response(clone!(@strong open_dialog, @weak self as this => move |_, response| {
            if response == gtk::ResponseType::Accept {
                let file = open_dialog.file().unwrap();
                this.window().open_file(&file);
            };
            open_dialog.destroy();
        }));
        open_dialog.show();
    }
}
