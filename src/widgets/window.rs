use crate::application::Application;
use crate::config::{APP_ID, PROFILE};
use crate::widgets::drawing_area::{DrawingArea, Filter};
use adw::subclass::prelude::*;
use anyhow::Result;
use gtk::{
    gio,
    gio::prelude::SettingsExt,
    glib::{self, clone},
    prelude::*,
    subclass::prelude::*,
    CompositeTemplate, Inhibit,
};
use gtk_macros::{action, get_action};
use log::error;

#[derive(PartialEq, Debug)]
pub enum View {
    Empty,
    Image,
}

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/belmoussaoui/Obfuscate/window.ui")]
    pub struct Window {
        pub settings: gio::Settings,
        #[template_child]
        pub drawing_area: TemplateChild<DrawingArea>,
        #[template_child]
        pub stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub window_title: TemplateChild<adw::WindowTitle>,
        #[template_child]
        pub zoom_level: TemplateChild<gtk::Label>,
        #[template_child]
        pub status_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub blur_btn: TemplateChild<gtk::ToggleButton>,
        #[template_child]
        pub filled_btn: TemplateChild<gtk::ToggleButton>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type ParentType = adw::ApplicationWindow;
        type Type = super::Window;

        fn new() -> Self {
            Self {
                settings: gio::Settings::new(APP_ID),
                drawing_area: TemplateChild::default(),
                stack: TemplateChild::default(),
                window_title: TemplateChild::default(),
                zoom_level: TemplateChild::default(),
                status_page: TemplateChild::default(),
                blur_btn: TemplateChild::default(),
                filled_btn: TemplateChild::default(),
            }
        }

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.install_action("win.zoom_in", None, move |widget, _, _| {
                let drawing_area = widget.imp().drawing_area.get();
                drawing_area.set_zoom(drawing_area.zoom() + 0.1);
            });

            klass.install_action("win.zoom_out", None, move |widget, _, _| {
                let drawing_area = widget.imp().drawing_area.get();
                drawing_area.set_zoom(drawing_area.zoom() - 0.1);
            });

            klass.install_action("win.zoom_reset", None, move |widget, _, _| {
                widget.imp().drawing_area.set_zoom(1.0);
            });

            klass.install_action("win.open", None, move |widget, _, _| {
                widget.application().open_file();
            });

            klass.install_action("win.copy", None, move |widget, _, _| {
                widget.imp().drawing_area.copy();
            });

            klass.install_action("win.paste", None, move |widget, _, _| {
                widget.imp().drawing_area.paste();
            });

            klass.install_action("win.save", None, move |widget, _, _| {
                let dialog = gtk::FileChooserNative::new(Some("Save File"), Some(widget), gtk::FileChooserAction::Save, Some("Save"), Some("Cancel"));
                dialog.set_modal(true);

                let png_filter = gtk::FileFilter::new();
                png_filter.set_name(Some("image/png"));
                png_filter.add_mime_type("image/png");
                dialog.add_filter(&png_filter);

                dialog.connect_response(clone!(@weak widget, @strong dialog => move |_, response| {
                    if response == gtk::ResponseType::Accept {
                        let file = dialog.file().unwrap();
                        if let Err(err) = widget.imp().drawing_area.save_to(&file) {
                            log::error!("Failed to save the image: {}", err);
                        }
                    }
                    dialog.destroy();
                }));
                dialog.show();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            if PROFILE == "Devel" {
                obj.style_context().add_class("devel");
            }
            gtk::Window::set_default_icon_name(APP_ID);
            obj.set_icon_name(Some(APP_ID));
            obj.setup_actions();
            obj.set_view(View::Empty);
            obj.load_state();
            self.status_page.set_icon_name(Some(APP_ID));
        }
    }

    impl WidgetImpl for Window {}
    impl WindowImpl for Window {
        fn close_request(&self, window: &Self::Type) -> Inhibit {
            if let Err(err) = window.save_state() {
                log::warn!("Failed to save window state {}", err);
            }
            self.parent_close_request(window)
        }
    }
    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, adw::ApplicationWindow,  gtk::ApplicationWindow, gio::ActionMap;
}

impl Window {
    pub fn new(app: &Application) -> Self {
        glib::Object::new(&[("application", app), ("default-width", &500), ("default-height", &400)]).unwrap()
    }

    pub fn set_view(&self, view: View) {
        let imp = self.imp();

        match view {
            View::Empty => {
                imp.stack.set_visible_child_name("empty");
            }
            View::Image => {
                imp.stack.set_visible_child_name("image");
            }
        }
        get_action!(self, @undo).set_enabled(false);
        get_action!(self, @redo).set_enabled(false);
        self.action_set_enabled("win.copy", view == View::Image);
        self.action_set_enabled("win.save", view == View::Image);
        self.action_set_enabled("win.zoom_in", view == View::Image);
        self.action_set_enabled("win.zoom_out", view == View::Image);
    }

    pub fn open_file(&self, file: &gio::File) {
        self.set_view(View::Empty);
        match self.set_open_file(file) {
            Ok(_) => self.set_view(View::Image),
            Err(err) => error!("Failed to open file {}", err),
        };
    }

    fn set_open_file(&self, file: &gio::File) -> Result<()> {
        let imp = self.imp();

        let info = file.query_info("standard::display-name", gio::FileQueryInfoFlags::NONE, gio::Cancellable::NONE)?;
        let display_name = info.attribute_as_string("standard::display-name");

        imp.window_title.set_subtitle(&display_name.unwrap());
        imp.drawing_area.load_file(file)?;
        Ok(())
    }

    fn application(&self) -> Application {
        self.property::<gtk::Application>("application").downcast().unwrap()
    }

    fn setup_actions(&self) {
        let imp = self.imp();
        let drawing_area = imp.drawing_area.get();
        self.action_set_enabled("win.zoom_reset", false);

        drawing_area.connect_notify_local(
            Some("zoom"),
            clone!(@weak self as this => move |drawing_area, _param| {
                let zoom = drawing_area.zoom();

                this.imp().zoom_level.set_text(&format!("{:.0}%", zoom * 100.0));
                this.action_set_enabled("win.zoom_in", zoom < 8.0);
                this.action_set_enabled("win.zoom_out", zoom > 0.1);
                this.action_set_enabled("win.zoom_reset", (zoom - 1.0).abs() > std::f64::EPSILON);
            }),
        );

        drawing_area.connect_local(
            "loaded",
            false,
            clone!(@weak self as this => @default-panic, move |_| {
                this.set_view(View::Image);

                None
            }),
        );

        // Undo
        action!(
            self,
            "undo",
            None,
            clone!(@weak drawing_area =>  move |_,  _| {
                drawing_area.undo();
            })
        );
        get_action!(self, @undo).set_enabled(false);
        // Redo
        action!(
            self,
            "redo",
            None,
            clone!(@weak drawing_area => move |_, _| {
                drawing_area.redo();
            })
        );
        get_action!(self, @redo).set_enabled(false);

        imp.blur_btn.bind_property("active", &*imp.filled_btn, "active").flags(glib::BindingFlags::INVERT_BOOLEAN).build();
        imp.filled_btn.bind_property("active", &*imp.blur_btn, "active").flags(glib::BindingFlags::INVERT_BOOLEAN).build();

        imp.blur_btn.connect_toggled(clone!(@weak drawing_area => move |toggle_btn| {
            if toggle_btn.is_active() {
                drawing_area.set_filter(Filter::Blur);
            }
        }));
        imp.filled_btn.connect_toggled(clone!(@weak drawing_area => move |toggle_btn| {
            if toggle_btn.is_active() {
                drawing_area.set_filter(Filter::Filled);
            }
        }));

        drawing_area
            .bind_property("can-undo", &get_action!(self, @undo), "enabled")
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();
        drawing_area
            .bind_property("can-redo", &get_action!(self, @redo), "enabled")
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();
    }

    fn load_state(&self) {
        let is_maximized = self.imp().settings.boolean("is-maximized");

        if is_maximized {
            self.maximize();
        }
    }

    fn save_state(&self) -> Result<(), glib::BoolError> {
        self.imp().settings.set_boolean("is-maximized", self.is_maximized())?;
        Ok(())
    }
}
